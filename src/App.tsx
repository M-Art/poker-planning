import React from 'react';
import './App.css';
import { useWindowSize } from './useWindowSize';

const cards = ["0", "1/2", "1", "2", "3", "5", "8", "13", "20", "40", "100", "∞", "?", "☕"]

const cardStyle = (windowWidth: number, windowHeight: number) => {
  const ratio = 1.5;
  let width = 0;
  let height = 0;

  if (windowWidth * ratio > windowHeight) {
    width = windowHeight / ratio;
    height = windowHeight;
  } else {
    width = windowWidth;
    height = windowWidth * ratio;
  }

  return {
    backgroundColor: "white",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    border: "2px solid #000",
    borderRadius: 0.05 * width,
    margin: "10px",
    scrollSnapAlign: "center",
    width: 0.9 * width,
    height: 0.9 * height,
    fontSize: `${0.2 * height}px`,
  };
}

const Card: React.FC<{name: string}> = (props) => {
  const windowSize = useWindowSize();

  return (
    <div style={cardStyle(windowSize.width || 0, windowSize.height || 0)}>
      <div className="Card-number">
        {props.name}
      </div>
    </div>
  );
}

const App: React.FC = () => {
  return (
    <div className="App">
      {cards.map(card => (
        <div key={card} className="App-card">
          <Card name={card}/>
        </div>
      ))}
    </div>
  );
}

export default App;